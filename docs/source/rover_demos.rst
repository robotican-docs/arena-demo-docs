 
================
Rover Demos
================

.. contents:: Table of Contents
    :local:



Waching the front camera video stream
-------------------------------------------

See Rrone Demos page for video streaming and watching examples.

The Rover launch file is rover.launch.py and streamer port is 6000+instance.

For example, to launch Arena with a Rover robot, run:

    .. code-block:: console

        ros2 launch arena_backend_demo rover.launch.py arena:=true


To view the video stream using Gstreamer:

    .. code-block:: console

        gst-launch-1.0 -v tcpclientsrc host=127.0.0.1 port=6001 ! gdpdepay ! videoconvert ! autovideosink
        





Control Rover by publishing Twist messages
-------------------------------------------


#. Launch Arena + Rover:
    
    .. code-block:: console

        ros2 launch arena_backend_demo drone.launch.py arena:=true


#. Publish Twist messages:

    .. code-block:: console

        ros2 topic pub /Rover001/cmd_vel geometry_msgs/msg/Twist "{linear: {x: 1.0, y: 0.0, z: 0.0}, angular: {x: 0.0, y: 0.0, z: 0.8}}"

The above command drives the robot in forward velocity of 1 m/s and angular velocity of 0.8 rad/s. The robot will continue to follow this command untill new command will be snt.


Control Rover from rqt Robot Steering
-------------------------------------


#. Launch Arena + Rover:
    
    .. code-block:: console

        ros2 launch arena_backend_demo drone.launch.py arena:=true


#. launch rqt_robot_steering and set to topic at the top to "/Rover001/cmd_vel"
    
    .. code-block:: console

        ros2 run rqt_robot_steering rqt_robot_steering

Now you can play with the linear and angular velocity sliders.




Running the SLAM demo from the slam_toolbox package
------------------------------------------------------

#. Install `slam_toolbox <https://github.com/SteveMacenski/slam_toolbox>`_:

    .. code-block:: console

        sudo apt install ros-foxy-slam-toolbox


#. Launch Arena + Rover + SLAM demo:

    .. code-block:: console

        ros2 launch arena_backend_demo rover_slam_demo.launch.py

Now you can drive the robot and watch the map build on rviz.

.. raw:: html

    <div style="position: relative; height: 0; overflow: hidden; max-width: 100%; height: auto; margin-bottom: 3em;">
    <video width="600" height="400" controls>
    <source src="_static/videos/roverslam.mp4" type="video/mp4">
    <source src="_static/videos/roverSlamb.webm" type="video/webm">
    Your browser does not support the video tag.
    </video>    
    </div>



Rover Backend Demo Code
------------------------------------------
This code is intended for creating a Rover and written as c++ code. 
This code located in the Backend_demo packege.

    .. code-block:: console

        #include <chrono>
        #include <cinttypes>
        #include <memory>

        #include "arena_common_interfaces/srv/spawn_actor.hpp"
        #include "rclcpp/rclcpp.hpp"

        using SpawnActor = arena_common_interfaces::srv::SpawnActor;


        int main(int argc, char *argv[])
        {
            rclcpp::init(argc, argv);
            auto node = rclcpp::Node::make_shared("rover_backend_node");
   
            auto player_param = node->declare_parameter("arena.player");
            auto possess_on_spawn_param = node->declare_parameter("rover.possess_on_spawn");
            auto use_tf_prefix_param = node->declare_parameter("rover.use_tf_prefix");
            auto camera_port_param = node->declare_parameter("rover.camera_port");
            auto camera_hfov_param = node->declare_parameter("rover.camera_hfov_deg");
            auto camera_gamma_param = node->declare_parameter("rover.camera_gamma");
            auto enable_2d_lidar_param = node->declare_parameter("rover.enable_2d_lidar");
            auto enable_3d_lidar_param = node->declare_parameter("rover.enable_3d_lidar");
            auto enable_odometry_param = node->declare_parameter("rover.enable_odometry");
   
            if (player_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                player_param.get_type() != rclcpp::ParameterType::PARAMETER_STRING or
                possess_on_spawn_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                possess_on_spawn_param.get_type() != rclcpp::ParameterType::PARAMETER_BOOL or
                use_tf_prefix_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                use_tf_prefix_param.get_type() != rclcpp::ParameterType::PARAMETER_BOOL or
                camera_port_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                camera_port_param.get_type() != rclcpp::ParameterType::PARAMETER_INTEGER or
                camera_hfov_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                camera_hfov_param.get_type() != rclcpp::ParameterType::PARAMETER_DOUBLE or
                camera_gamma_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                camera_gamma_param.get_type() != rclcpp::ParameterType::PARAMETER_DOUBLE or
                enable_2d_lidar_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                enable_2d_lidar_param.get_type() != rclcpp::ParameterType::PARAMETER_BOOL or
                enable_3d_lidar_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                enable_3d_lidar_param.get_type() != rclcpp::ParameterType::PARAMETER_BOOL or
                enable_odometry_param.get_type() == rclcpp::PARAMETER_NOT_SET or
                enable_odometry_param.get_type() != rclcpp::ParameterType::PARAMETER_BOOL)
            {
                RCLCPP_ERROR(node->get_logger(), "Parameters initialization failed");
                return -1;
            }
   
            auto srv = "/arena/" + player_param.get<std::string>() + "/spawn";
            auto client = node->create_client<SpawnActor>(srv);
            while (!client->wait_for_service(std::chrono::seconds(1)))
            {
                if (!rclcpp::ok())
                {
                    RCLCPP_ERROR(node->get_logger(), "Client interrupted while waiting for service to appear.");
                    return 1;
                }
                RCLCPP_INFO(node->get_logger(), "Waiting for service [ " + srv + "] to appear...");
            }
            auto request = std::make_shared<SpawnActor::Request>();
            request->type = "Rover";
            std::string id = node->get_namespace();
            id.erase(0, 1);
            request->transform.header.frame_id = id;
            request->options = "camera_port=" + std::to_string(camera_port_param.get<int>()) +
                            " camera_hfov=" + std::to_string(camera_hfov_param.get<double>()) +
                            " camera_gamma=" + std::to_string(camera_gamma_param.get<double>()) +
                            " possess=" + (possess_on_spawn_param.get<bool>() ? "true" : "false") +
                            " use_tf_prefix=" + (use_tf_prefix_param.get<bool>() ? "true" : "false") +
                            " enable1_odometry=" + (enable_odometry_param.get<bool>() ? "true" : "false") +
                            " enable_3d_lidar=" + (enable_3d_lidar_param.get<bool>() ? "true" : "false") +
                            " enable_2d_lidar=" + (enable_2d_lidar_param.get<bool>() ? "true" : "false");
        
            std::cout << "Spawning Rover with options: " << request->options << std::endl;
   
            auto result_future = client->async_send_request(request);
            if (rclcpp::spin_until_future_complete(node, result_future) !=
                rclcpp::FutureReturnCode::SUCCESS)
            {
                RCLCPP_ERROR(node->get_logger(), "Service call failed :(");
                return 1;
            }
            auto result = result_future.get();
            if (result->success)
            {
                RCLCPP_INFO(node->get_logger(), "Spawn Succeeded");
            }
            else
            {
                RCLCPP_ERROR(node->get_logger(), "Spawn failed, %s", result->message.c_str());
            }
        
            rclcpp::spin(node);
            rclcpp::shutdown();
            return 0;
        }
Examine the code
```````````
pay attantion to the dependencies you should to add at the package's packege.xml file:
    .. code-block:: console

        <depend>arena_backend_common</depend>
        <depend>arena_common_interfaces</depend>

Furthermore, add the Arena's find package lines to the package's CMakeList.txt file : 

    .. code-block:: console

        find_package(arena_backend_common REQUIRED)
        find_package(arena_common_interfaces REQUIRED)


At the Node's cpp file add the Arena's include:

    .. code-block:: console

        #include "arena_common_interfaces/srv/spawn_actor.hpp"
        #include "rclcpp/rclcpp.hpp"


Add the using of the spawn_actor service to the file:

    .. code-block:: console

        using SpawnActor = arena_common_interfaces::srv::SpawnActor;


At the cpp file there is auto declaration of the Rover's parameters. This parameters are taken from the yaml file in the params folder.

    .. code-block:: console

        auto player_param = node->declare_parameter("arena.player");
        auto possess_on_spawn_param = node->declare_parameter("rover.possess_on_spawn");
        auto use_tf_prefix_param = node->declare_parameter("rover.use_tf_prefix");
        auto camera_port_param = node->declare_parameter("rover.camera_port");
        auto camera_hfov_param = node->declare_parameter("rover.camera_hfov_deg");
        auto camera_gamma_param = node->declare_parameter("rover.camera_gamma");
        auto enable_2d_lidar_param = node->declare_parameter("rover.enable_2d_lidar");
        auto enable_3d_lidar_param = node->declare_parameter("rover.enable_3d_lidar");
        auto enable_odometry_param = node->declare_parameter("rover.enable_odometry");

After the  params decleration there is a params type check. 
The Service and Clinet create and the waiting for the service:

   .. code-block:: console

        auto srv = "/arena/" + player_param.get<std::string>() + "/spawn";
        auto client = node->create_client<SpawnActor>(srv);

The request for the Rover's spawn:

 .. code-block:: console

        auto request = std::make_shared<SpawnActor::Request>();
        request->type = "Rover";
        std::string id = node->get_namespace();
        id.erase(0, 1);
        request->transform.header.frame_id = id;
        request->options = "camera_port=" + std::to_string(camera_port_param.get<int>()) +
                       " camera_hfov=" + std::to_string(camera_hfov_param.get<double>()) +
                       " camera_gamma=" + std::to_string(camera_gamma_param.get<double>()) +
                       " possess=" + (possess_on_spawn_param.get<bool>() ? "true" : "false") +
                       " use_tf_prefix=" + (use_tf_prefix_param.get<bool>() ? "true" : "false") +
                       " enable1_odometry=" + (enable_odometry_param.get<bool>() ? "true" :  "false") +
                       " enable_3d_lidar=" + (enable_3d_lidar_param.get<bool>() ? "true" : "false") +
                       " enable_2d_lidar=" + (enable_2d_lidar_param.get<bool>() ? "true" :   "false");
   
        std::cout << "Spawning Rover with options: " << request->options << std::endl;


After the request part there is a check for the success of the Rover spawn service. 
    .. code-block:: console

       auto result_future = client->async_send_request(request);
       if (rclcpp::spin_until_future_complete(node, result_future) !=
            rclcpp::FutureReturnCode::SUCCESS)
        {
            RCLCPP_ERROR(node->get_logger(), "Service call failed :(");
            return 1;
        }
        auto result = result_future.get();
       if (result->success)
        {
            RCLCPP_INFO(node->get_logger(), "Spawn Succeeded");
        }
       else
        {
            RCLCPP_ERROR(node->get_logger(), "Spawn failed, %s", result->message.c_str());
        }
        


Control Rover using ROS2 navigation2 stack
------------------------------------------

Comming soon...
