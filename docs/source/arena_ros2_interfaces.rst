 
..  _arena_ros2_interfaces:

======================
Arena ROS2 Interfaces
======================

.. contents:: Table of Contents
    :local:

Coordinate Frames
------------------

Arena uses the same ROS2 units and coordinate conventions, see `REP 103 <https://repo.test.ros2.org/docs/independent/api/rep/html/rep-0103.html>`_.


Each world contains an anchor point which is fixed at ground level and serves as the level origin.

When hosting a level, the global geographic position can be set.

The origin global position is used for sensors simulation like GPS.

The origin coordinates are published in the status topic, see bellow.


Published topics
----------------

#. **/arena/<user_name>/status - arena_common_interfaces/ArenaStatus**

This message is published at a frequency of 5 Hz and contains information about the current level such as the name of the level, the update frequency (fps), the list of users, the list of robots and the global location of the origin.


#. **/<robot_name>/arena/state - arena_common_interfaces/ArenaPawnState**


Most robots publish this message which contains ground truth information about position, orientation and velocity in the level axis system (origin).

This message also includes additional arrays that allow each robot to specify its unique characteristics.


Subscribed topics
-----------------

#. **/arena/<user_name>/draw_debug -** `visualization_msgs/Marker <http://docs.ros.org/en/noetic/api/visualization_msgs/html/msg/Marker.html>`_

Pוublishing messages to this topic allows you to draw 3D lines in different colors on top of the environment

Currently, only the LINE_STRIP (4) type is supported.

See `rviz/DisplayTypes/Marker <http://wiki.ros.org/rviz/DisplayTypes/Marker#Line_Strip_.28LINE_STRIP.3D4.29>`_ Wiki for more details.


#. **/<robot_name>/arena/set_state - arena_common_interfaces/ArenaPawnState**

Kinematic robots such as drones that use a backend to calculate their movement use this topic to position and control the robot in the simulation environment. 
The additional arrays present in the message allow control of the unique characteristics of each robot such as the gimbal aiming for example.


Services
--------

#. **/arena/<user_name>/spawn - arena_common_interfaces/SpawnActor**

This service is used to spawn robots. Proper usage requires specifying the type (e.g. Drone, Rover) of robot in the type field and specifying the name of the robot in the transform.header.frame_id field.
The transform field allows selection of the location and orientation of the robot in space in relation to the level origin frame.
The options string allows setting robot specific parameters.

#. **/arena/<user_name>/destroy - arena_common_interfaces/DestroyActor**

This service is used to destroy robots by specifying thier name.